package com.gdziepotanczymy.service.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

//@Builder
@Getter
@Setter
@NoArgsConstructor
//@AllArgsConstructor
@SuperBuilder
public class CreateUpdateOrganizerDto extends CreateUpdateUserDto{

}
